import React, {useState} from 'react';
// import logo from './logo.svg';
import './App.css';
import PersonnelRow from './components/PersonnelRow';
import uuid from 'uuid/v4';
import NewPersonnelForm from './components/newPersonnelForm';

function App(){


  let [redTeam, setRedTeam] = useState([]);

  let [blueTeam, setBlueTeam] = useState([]);

  let [personnel, setPersonnel] = useState();


    // [

    // {id:uuid(),name:"Peter", job: "Plumber"},
    // {id:uuid(),name:"Lex", job: "Construction Worker"},
    // {id:uuid(),name:"James", job: "Trainer"},
    // {id:uuid(),name:"Sasha", job: "Bartender"},
    // {id:uuid(),name:"Maria", job: "Student"},
    // ]);

   // let blueTeamDisplay = (personnel.length > 0)
  
  let [redTeam, setRedTeam] = useState([]);

  let [blueTeam, setBlueTeam] = useState([]);

  let [displayForm, setDisplayForm] = useState(false);  


  let redTeamDisplay = redTeam.map( function(member) 
  {
    return (
      <li key={member.id}>
      {member.name}<br/>
      <button onClick={function (){removeFromRed(member)}}>Remove</button>
      </li>
    );
  });

  
  let blueTeamDisplay = blueTeam.map(function(member) 
  {
    return (
      <li key={member.id}>
      {member.name}<br/>
      <button onClick={function (){removeFromBlue(member)}}>Remove</button>
      </li>
    );
  });


  function tableHeader()
  {
    return(
      <thead>
        <tr>
          <th>Id</th>
          <th>Name</th>
          <th>Job</th>
          <th>Action</th>
      </tr>
      </thead>
    );
  }

  let tbody = personnel.map(function({id, name, job}) 
  {
    return <PersonnelRow 
      key = {id} 
      id = {id} 
      name = {name} 
      job = {job} 
      addToRedTeam = {addToRedTeam}
      addToBlueTeam = {addToBlueTeam}
    />
  });

  function addPersonnel(newPersonnel){
    setPersonnel([...personnel, newPersonnel])
  }

  function addToRedTeam(newMember)
  {
    setRedTeam([...redTeam, newMember]);

    let newPersonnel = personnel.filter(function(person){
      if(person.id !== newMember.id){
        return person;
      }
    });
    setPersonnel(newPersonnel);
  }


  function addToBlueTeam(newMember)
  {
    setBlueTeam([...blueTeam, newMember]);

    let newPersonnel = personnel.filter( function(person){
      if(person.id !== newMember.id){
        return person;
      }
    });
    setPersonnel(newPersonnel);
  }

  function removeFromRed(member)  {
    setPersonnel([...personnel, member])
    let newRedTeam = redTeam.filter( function(person){
      if(person.id !== member.id){
        return person;
      }
    }); 
      setRedTeam(newRedTeam)
  }


  function removeFromBlue(member)  {
    setPersonnel([...personnel, member])
    let newBlueTeam = blueTeam.filter( function(person){
      if(person.id !== member.id){
        return person;
      }
    });

    setBlueTeam(newBlueTeam)
  }

  function showTable()
  {
  if(personnel.length > 0)
  {      
    return (
      <table className = "table table-striped">
       
        {tableHeader()}

        <tbody>
          {tbody}
        </tbody>

      </table>

      )    
    }

    else

    { 
      return(
      <div className="jumbotron container-fluid">
        <h3>No personnel to display.</h3>
          <p>You may add personnel data</p>
      </div>)
    }
  }
    function showForm(){
      if(displayForm){
        return(
            <NewPersonnelForm a={addPersonnel} b={hideFormBtnClick}
            );
          }else{
            
          })
        
        
    return( 
      <div className="App container-fluid">
      <div className="row">
        <NewPersonnelForm addPersonnel={addPersonnel}/>
  
          {showTable()}
      <div className ="row text-center">

      <div className ="col-6 text-center table-primary">
        
      <div className="col-12 col-md-8 offset-md-2">
      <div className="row mt-5">
      <div className="col-12 col-md-8 offset-md-2">

        <h2>Red Team</h2>
     
          <ul>
            {redTeamDisplay}
          </ul>
      </div> 

      <div className="col-6 text-center table-secondary">
        <h2>Blue Team</h2>
          <ul>
            {blueTeamDisplay}
          </ul>
      </div>
  
    </div>
  </div>    
  );
}

export default App;
