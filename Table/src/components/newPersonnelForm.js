import React, {useState} from 'react';
import uuid from 'uuid/v4';

function NewPersonnelForm(props){
	let [name, setName]= useState("");

	let [job, setJob] = useState("");

	let [nameError, setNameError] = useState("");

	let [jobError, setJobError] = useState("");

	let [addBtnDisabled, setAddBtnDisabled] = useState(true);

	
	function nameChangeHandler(event){
		setName(event.target.value);

		if(event.taget.value.trim() === ""){
			setNameError("This field cannot be empty.");
	}else{
		setNameError("");
	}

}

	function jobChangeHandler(event){
		setJob(event.target.value);

		if(event.taget.value.trim()===""){
			setJobError("This field cannot be empty.")
		}else{
			setJobError("");
		}
	}
	checkForm();


	function checkForm(){
		if(name!=="" && job!==""){
			if(addBtnDisabled!==false){
			setAddBtnDisabled(false);
		}
		} else {
			
			if(addBtnDisabled!==true){
			setAddBtnDisabled(true);
			}
		}
	}


	function addClickHandler(){
		
	 let newPersonnel = {
	 	id: uuid(),
	 	name: name,
	 	job: job,
	}

	props.addPersonnel(newPersonnel);
		
		setName("");
		setJob("");
	}


	return(
		<form>
			<div className="form-group">
				
				<label htmlFor="name">Name</label>
				<input
				onChange={nameChangeHandler}
				onBlur={nameChangeHandler}
				className="form-control text-center"
				type="text" 
				id="name"
				autoComplete="off"
				value={name}
				/>
				<small className="text-danger">{nameError}</small>
			</div>
			
			<div className="form-group">
				
				<label htmlFor="name">Job</label>
				<input
				onChange={nameChangeHandler}
				onBlur={jobChangeHandler}
				className="form-control"
				type="text" 
				id="job"
				autoComplete="off"
				value={job}
				/>
				
				<small className="text-danger">{jobError}</small>	
			</div>
				<button disabled={addBtnDisabled} onClick={addClickHandler} type="button" className="btn btn-success">
					Add Personnel
				</button>

				<button addClickhandler={}
				
		</form>
	)
}

export default NewPersonnelForm;
