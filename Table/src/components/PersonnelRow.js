import React from 'react';

function PersonnelRow(props){
	function redBtnClicked()
	{
		addToRedTeam({id: id, name: name, job: job})
	}
	function blueBtnClicked()
	{
		addToBlueTeam({id: id, name: name, job: job})
	}



	return(
		<tr>
			<td>{props.id}</td>
			<td>{props.name}</td>
			<td>{props.job}</td>

			<td>
				<button onClick={redBtnClicked}
				className="btn btn-danger">Add to Red Team
				</button>

				<button onClick={blueBtnClicked}
				className="btn btn-primary">Add to Red Team
				</button>
			</td>
		</tr>
	);
}

export default PersonnelRow;